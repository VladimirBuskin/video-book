// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


var videoId = 'H1Mzqdig-4w';
//var videoId = 'bo_efYhYU2A';


// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '390',
    width: '640',
    videoId: videoId,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  var time = +(localStorage['video_' + videoId] || "0");
  player.seekTo(time);
  player.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  if (!done) {
    saveTime();
    done = true;
  }
}

function saveTime() {
  var time = player.getCurrentTime();
  localStorage['video_' + videoId] = time;
  console.log('remembered: ' + time);
  setTimeout(saveTime, 1000);
}
